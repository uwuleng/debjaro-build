#!/usr/bin/env bash
#### Configure system
apt-get install curl gnupg -y

echo "deb https://pkgmaster.devuan.org/merged testing main contrib non-free" > /etc/apt/sources.list

apt-get update -y
apt-get dist-upgrade -y

cat > /etc/apt/apt.conf.d/01norecommend << EOF
APT::Install-Recommends "0";
APT::Install-Suggests "0";
EOF

curl https://liquorix.net/liquorix-keyring.gpg | apt-key add -
echo "deb http://liquorix.net/debian testing main" > /etc/apt/sources.list.d/liquorix.list
apt-get update -y
apt-get install linux-image-liquorix-amd64 linux-headers-liquorix-amd64 -y

apt-get install xserver-xorg xinit -y

echo "deb https://raw.githubusercontent.com/lxde-gtk3/binary-packages/master stable main" > /etc/apt/sources.list.d/lxde-gtk3.list
curl https://raw.githubusercontent.com/lxde-gtk3/binary-packages/master/dists/stable/Release.key | apt-key add -
apt-get update
apt-get install lxde-core lxterminal -y

apt-get install lightdm lightdm-gtk-greeter -y

