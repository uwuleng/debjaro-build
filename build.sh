set -ex
git clone https://gitlab.com/tearch-linux/applications-and-tools/teaiso
cd teaiso
apt install xorriso grub-pc-bin grub-efi mtools make python3 \
    dosfstools e2fsprogs squashfs-tools python3-yaml \
    gcc wget curl unzip xz-utils -y
make
exec ./mkteaiso --profile=$(pwd)/../debjaro --output=/builds/uwuleng/debjaro-build/output/ --debug 2>&1
